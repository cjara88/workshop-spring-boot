package com.example.workshop.exception;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = -3929158965424163863L;
	
	public ApiException(String message) {
		super(message);
	}
}
