package com.example.workshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.workshop.model.Pais;

public interface PaisRepository extends JpaRepository<Pais, Long> {


}
