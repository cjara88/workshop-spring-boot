package com.example.workshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.workshop.model.Cuenta;
import com.example.workshop.model.CuentaId;

public interface CuentaRepository extends JpaRepository<Cuenta, CuentaId> {
	
}
