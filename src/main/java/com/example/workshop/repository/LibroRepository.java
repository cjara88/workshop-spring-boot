package com.example.workshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.workshop.model.Libro;
import com.example.workshop.model.LibroId;

public interface LibroRepository extends JpaRepository<Libro, LibroId> {


}
