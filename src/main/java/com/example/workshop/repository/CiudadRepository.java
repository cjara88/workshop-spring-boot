package com.example.workshop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.workshop.model.Ciudad;

public interface CiudadRepository extends PagingAndSortingRepository<Ciudad, Long> {

	List<Ciudad> findAll();
		
	List<Ciudad> findByPaisId(Long idPais);
	
	List<Ciudad> findByNombreContainingIgnoreCase(String nombre);
	
	//Utizando JPQL
	@Query("select c from Ciudad c where c.pais.id = ?1")
	List<Ciudad> findByPaisIdWithQuery(Long idPais);
	
	//Utilizando @Param para definir el nombre de idCountry
	@Query("select c from Ciudad c where c.pais.id = :idCountry")
	List<Ciudad> findByPaisIdWithQueryParamName(@Param("idCountry") Long idPais);
	
	//Utilizando query nativo
	@Query(value = "SELECT * FROM Ciudad c WHERE c.pais_id = ?1", nativeQuery = true)
	List<Ciudad> findByPaisIdWithNativeQuery(Long idPais);
	
}
