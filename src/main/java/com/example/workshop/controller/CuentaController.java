package com.example.workshop.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.workshop.model.Cuenta;
import com.example.workshop.service.CuentaService;

import io.swagger.annotations.Api;

@Api(tags = "Cuentas")
@RestController
@RequestMapping("/cuentas")
public class CuentaController {
	
	@Autowired
	CuentaService cuentaService;
	
	/**
	 * Ejemplo de obtención de una cuenta por su clave compuesta
	 * @param numeroCuenta
	 * @param tipoCuenta
	 * @return {@link Cuenta}
	 */
	@GetMapping("{numeroCuenta}/{tipoCuenta}")
	Optional<Cuenta> findById(@PathVariable("numeroCuenta") Long numeroCuenta,
			@PathVariable("tipoCuenta") String tipoCuenta) {
		return cuentaService.findById(numeroCuenta, tipoCuenta);
	}

}
