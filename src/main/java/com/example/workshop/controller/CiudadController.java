package com.example.workshop.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.workshop.model.Ciudad;
import com.example.workshop.repository.CiudadRepository;
import com.example.workshop.service.CiudadService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin
@RestController
@RequestMapping("/ciudades")
@Api(tags = "Ciudades")
public class CiudadController {

	@Autowired
	CiudadService ciudadService;
	
	@Autowired
	CiudadRepository ciudadRepository;

	@GetMapping
	@ApiOperation("Retorna una lista de ciudades")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operación exitosa."),
			@ApiResponse(code = 401, message = "No se encuentra autorizado."),
			@ApiResponse(code = 403, message = "Acceso prohibido al recurso."),
			@ApiResponse(code = 404, message = "No se encuentra el recurso") })
	List<Ciudad> findAll() {
		return ciudadService.findAll();
	}

	@GetMapping("{id}")
	@ApiOperation("Obtiene una ciudad por ID")
	Optional<Ciudad> getById(@ApiParam(value = "ID de la ciudad") @PathVariable("id") Long id) {
		return ciudadService.getById(id);
	}
	
	/**
	 * Ejemplo de uso de transacción
	 */
	@GetMapping("transactional")
	@ApiOperation("Para prueba de transacciones")
	void pruebaTransactional() {
		 ciudadService.pruebaTransactional();;
	}
	
	/**
	 * Ejemplo de busqueda de {@link Ciudad} por nombre
	 * @param nombre
	 * @return List<Ciudad>
	 */
	@GetMapping("findByNombre/{nombre}")
	@ApiOperation("Busca una ciudad por nombre")
	List<Ciudad> findByNombreContaining(@ApiParam(value = "Nombre de ciudad") @PathVariable("nombre") String nombre) {
		return ciudadRepository.findByNombreContainingIgnoreCase(nombre);
	}
	
	/**
	 * Ejemplo de paginado de {@link Ciudad}
	 * @param page
	 * @param pageSize
	 * @return Page<Ciudad>
	 */
	@GetMapping("page")
	@ApiOperation(value = "Retorna una lista paginada de ciudades")
	Page<Ciudad> getPage(@ApiParam("Número de pagina") @RequestParam("page") int page, @ApiParam(value = "Tamaño del paginado") @RequestParam("pageSize") int pageSize) {
//		 Pageable pageable = PageRequest.of(1, 10, Sort.by("nombre").ascending());
		return ciudadService.getPage(PageRequest.of(page, pageSize));
	}

	@PostMapping
	@ApiOperation(value = "Persiste una nueva ciudad")
	Ciudad create(@ApiParam("Nueva Ciudad") @RequestBody @Valid Ciudad city) {
		return ciudadService.create(city);
	}

	@PutMapping
	@ApiOperation(value = "Actualiza los valores de una ciudad")
	Ciudad update(@ApiParam("Ciudad a actualizar") @RequestBody Ciudad ciudad) {
		return ciudadService.update(ciudad);

	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Elimina una ciudad")
	void delete(@ApiParam("ID de Ciudad") @PathVariable Long id) {
		ciudadService.delete(id);
		;
	}
	
	//Una de las formas de manejo de errores.
//
//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	@ExceptionHandler(MethodArgumentNotValidException.class)
//	public Map<String, String> handleValidationExceptions(
//	  MethodArgumentNotValidException ex) {
//	    Map<String, String> errors = new HashMap<>();
//	    ex.getBindingResult().getAllErrors().forEach((error) -> {
//	        String fieldName = ((FieldError) error).getField();
//	        String errorMessage = error.getDefaultMessage();
//	        errors.put(fieldName, errorMessage);
//	    });
//	    return errors;
//	}

}
