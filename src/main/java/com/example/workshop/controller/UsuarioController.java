package com.example.workshop.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.workshop.model.Usuario;

import io.swagger.annotations.Api;

@Api(tags = "Usuarios")
@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	/**
	 * Ejemplo de funcionamiento de validaciones sobre la entidad.
	 * 
	 * @param usuario
	 * @return String
	 */
	@PostMapping
	ResponseEntity<String> addUser(@Valid @RequestBody Usuario usuario) {
		return ResponseEntity.ok("El usuario es válido");
	}

}
