package com.example.workshop.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.workshop.model.Ciudad;
import com.example.workshop.model.Pais;
import com.example.workshop.repository.CiudadRepository;
import com.example.workshop.service.PaisService;

import io.swagger.annotations.Api;

@Api(tags = "Paises")
@RestController
@RequestMapping("/paises")
public class PaisController {

	@Autowired
	PaisService paisService;

	@Autowired
	CiudadRepository cityRepository;

	@GetMapping
	List<Pais> findAll() {
		return paisService.findAll();
	}

	@GetMapping("{id}")
	Optional<Pais> getById(@PathVariable("id") Long id) {
		return paisService.findById(id);
	}

	/**
	 * Ejemplo de obtención de sub-recursos, se lista las ciudades de un pais
	 * 
	 * url = http://localhost:8080/paises/1/ciudades?tipoConsulta=JPA
	 * @param id ID del Pais
	 * @param tipoConsulta JPA, JPQL, JPQL_WITH_NAME_PARAM, NATIVE, JPA
	 * @return List<Ciudad>
	 */
	@GetMapping("/{id}/ciudades")
	List<Ciudad> findCiudadesById(@PathVariable("id") Long id,
			@RequestParam(name = "tipoConsulta", defaultValue = "JPA") String tipoConsulta) {

		List<Ciudad> listaCiudad = new ArrayList<>();

		switch (tipoConsulta) {
		case "JPQL":
			listaCiudad = cityRepository.findByPaisIdWithQuery(id);
			break;
		case "JPQL_WITH_NAME_PARAM":
			listaCiudad = cityRepository.findByPaisIdWithQueryParamName(id);
			break;
		case "NATIVE":
			listaCiudad = cityRepository.findByPaisIdWithNativeQuery(id);
			break;
		case "JPA":
			listaCiudad = cityRepository.findByPaisId(id);
			break;
		}

		return listaCiudad;
	}

	@PostMapping
	Pais create(@RequestBody Pais pais) {
		return paisService.create(pais);
	}

	@PutMapping
	Pais update(@RequestBody Pais nuevoPais) {
		return paisService.update(nuevoPais);

	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		paisService.deleteById(id);
	}

}
