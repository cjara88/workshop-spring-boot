package com.example.workshop.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.workshop.model.Libro;
import com.example.workshop.service.LibroService;

import io.swagger.annotations.Api;

@Api(tags = "Libros")
@RestController
@RequestMapping("/libros")
public class LibroController {

	@Autowired
	LibroService libroService;

	/**
	 * Ejemplo de obtención de un {@link Libro} por su clave compuesta
	 * 
	 * @param titulo
	 * @param idioma
	 * @return {@link Libro}
	 */
	@GetMapping("{titulo}/{idioma}")
	Optional<Libro> findById(@PathVariable("titulo") String titulo, @PathVariable("idioma") String idioma) {
		return libroService.findById(titulo, idioma);
	}

}
