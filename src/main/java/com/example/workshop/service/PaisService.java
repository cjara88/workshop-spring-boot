package com.example.workshop.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.workshop.model.Pais;
import com.example.workshop.repository.PaisRepository;

@Service
public class PaisService {

	@Autowired
	PaisRepository paisRepository;

	public List<Pais> findAll() {
		return paisRepository.findAll();
	}

	public Page<Pais> getPage(Pageable pageable) {
		return paisRepository.findAll(pageable);
	}

	public Optional<Pais> findById(Long id) {
		return paisRepository.findById(id);
	}

	public Pais create(Pais pais) {
		pais.setFechaCreacion(new Date());
		return paisRepository.save(pais);
	}

	public Pais update(Pais pais) {
		pais.setFechaModificacion(new Date());
		return paisRepository.save(pais);

	}

	public void deleteById(Long id) {
		paisRepository.deleteById(id);
	}

}
