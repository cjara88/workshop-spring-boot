package com.example.workshop.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.workshop.model.Libro;
import com.example.workshop.model.LibroId;
import com.example.workshop.repository.LibroRepository;

@Service
public class LibroService {

	@Autowired
	LibroRepository libroRepository;
	
	public Optional<Libro> findById(String titulo, String idioma) {
		return libroRepository.findById(new LibroId(titulo, idioma));
	}

}
