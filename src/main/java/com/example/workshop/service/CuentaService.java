package com.example.workshop.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.workshop.model.Cuenta;
import com.example.workshop.model.CuentaId;
import com.example.workshop.repository.CuentaRepository;

@Service
public class CuentaService {

	@Autowired
	CuentaRepository cuentaRepository;
	
	public Optional<Cuenta> findById(Long numeroCuenta, String tipoCuenta) {
		return cuentaRepository.findById(new CuentaId(numeroCuenta, tipoCuenta));
	}

}
