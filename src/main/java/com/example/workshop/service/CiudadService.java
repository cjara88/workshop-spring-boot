package com.example.workshop.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.workshop.exception.ApiException;
import com.example.workshop.model.Ciudad;
import com.example.workshop.model.Pais;
import com.example.workshop.repository.CiudadRepository;
import com.example.workshop.repository.PaisRepository;

@Service
public class CiudadService {

	@Autowired
	CiudadRepository ciudadRepository;
	
	@Autowired
	PaisRepository paisRepository;
	
	public List<Ciudad> findAll() {
		return ciudadRepository.findAll();
	}
	
	public Page<Ciudad> getPage(Pageable pageable) {
		return ciudadRepository.findAll(pageable);
	}
	
	/**
	 * Prueba de Rollback de Transacción
	 * utilizando la anotacion @Transactional
	 */
	@Transactional
	public void pruebaTransactional() {
		Pais pais = new Pais();
		pais.setNombre("Pais Transaccional");
		paisRepository.save(pais);
		
		/*
		 * Al lanzar la excepción el pais persistido previamente no debe aparecer en la
		 * base de datos.
		 * Si comentamos todo el bloque if siguiente, deberia almacenar tanto pais
		 * como ciudad normalmente.
		 */
		if (pais.getId() != null) {
			throw new ApiException("Error controlado.");
		}
		
		Ciudad ciudad = new Ciudad();
		ciudad.setNombre("Ciudad Transaccional");
		ciudad.setPais(pais);
		ciudadRepository.save(ciudad);
	}
	
	public Optional<Ciudad> getById(@PathVariable("id") Long id) {
		return ciudadRepository.findById(id);
	}
	
	public Ciudad create(Ciudad city) {
		return ciudadRepository.save(city);
	}
	
	public Ciudad update(Ciudad nuevaCiudad) {
		return ciudadRepository.save(nuevaCiudad);
		
	}

	public void delete(Long id) {
		ciudadRepository.deleteById(id);;
	}
	
}
