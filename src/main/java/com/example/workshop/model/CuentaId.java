package com.example.workshop.model;

import java.io.Serializable;

/**
 * Clave compuesta de {@link Cuenta}
 * @author cjara
 *
 */
public class CuentaId implements Serializable {

	private static final long serialVersionUID = -8918970921013591856L;

	private Long numeroCuenta;

	private String tipoCuenta;

	public CuentaId() {

	}

	public CuentaId(Long numeroCuenta, String tipoCuenta) {
		super();
		this.numeroCuenta = numeroCuenta;
		this.tipoCuenta = tipoCuenta;
	}



	public Long getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(Long numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroCuenta == null) ? 0 : numeroCuenta.hashCode());
		result = prime * result + ((tipoCuenta == null) ? 0 : tipoCuenta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuentaId other = (CuentaId) obj;
		if (numeroCuenta == null) {
			if (other.numeroCuenta != null)
				return false;
		} else if (!numeroCuenta.equals(other.numeroCuenta))
			return false;
		if (tipoCuenta == null) {
			if (other.tipoCuenta != null)
				return false;
		} else if (!tipoCuenta.equals(other.tipoCuenta))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CuentaId [numeroCuenta=" + numeroCuenta + ", tipoCuenta=" + tipoCuenta + "]";
	}

}
