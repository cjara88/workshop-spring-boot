package com.example.workshop.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "ciudad")
public class Ciudad implements Serializable {

	private static final long serialVersionUID = 5677450425113708867L;

	@Id
	//Ejemplo de generación con sequencia
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ciudad_generator")
	@SequenceGenerator(name="ciudad_generator", sequenceName = "ciudad_seq")
	private Long id;

	@NotBlank(message = "Nombre es un campo requerido.")
	private String nombre;
	
	//Indica que no va ser serilizado
	@JsonBackReference
	@ManyToOne	
	private Pais pais;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "Ciudad [id=" + id + ", nombre=" + nombre + "]";
	}

	
}
