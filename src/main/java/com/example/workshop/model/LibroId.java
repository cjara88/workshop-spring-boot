package com.example.workshop.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * Clave compuesta de {@link Libro}
 * @author cjara
 *
 */
@Embeddable
public class LibroId implements Serializable {

	private static final long serialVersionUID = 8492508912782614040L;

	private String titulo;

	private String idioma;

	public LibroId() {
	}
	
	public LibroId(String titulo, String idioma) {
		super();
		this.titulo = titulo;
		this.idioma = idioma;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idioma == null) ? 0 : idioma.hashCode());
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LibroId other = (LibroId) obj;
		if (idioma == null) {
			if (other.idioma != null)
				return false;
		} else if (!idioma.equals(other.idioma))
			return false;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LibroId [titulo=" + titulo + ", idioma=" + idioma + "]";
	}

}
