package com.example.workshop.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Ejemplo de entidad con clave compuesta utilizando la anotación @IdClass
 * 
 * @author cjara
 *
 */
@Entity
@Table(name = "cuenta")
@IdClass(CuentaId.class)
public class Cuenta implements Serializable {

	private static final long serialVersionUID = 6133819501756138613L;

	@Id
	private Long numeroCuenta;

	@Id
	private String tipoCuenta;

	private String descripcion;

	public Long getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(Long numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Cuenta [numeroCuenta=" + numeroCuenta + ", tipoCuenta=" + tipoCuenta + "]";
	}

}
