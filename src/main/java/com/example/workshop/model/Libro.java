package com.example.workshop.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Ejemplo de entidad con clave compuesta utilizando la anotación @EmbeddedId
 * 
 * @author cjara
 *
 */
@Entity
@Table(name = "libro")
public class Libro implements Serializable {

	private static final long serialVersionUID = -4116367580489633299L;

	@EmbeddedId
	private LibroId libroId;


	public LibroId getLibroId() {
		return libroId;
	}


	public void setLibroId(LibroId libroId) {
		this.libroId = libroId;
	}


	@Override
	public String toString() {
		return "Libro [libroId=" + libroId + "]";
	}
	
}
