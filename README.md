Spring Boot Workshop
----------------------------------------------

Repositorio con ejemplos realizados en el workshop.


#### Requisitos

* OpenJDK 1.8+
* Maven 3.3+
* IDE
* PostgreSQL 9.4+


#### Configuraciones

Crear una base de datos en PostreSQL, posteriormente actualizar en  `application.properties` los siguientes datos

```
spring.datasource.url=jdbc:postgresql://localhost:5432/<base-datos>
spring.datasource.username=<usuario>
spring.datasource.password=<password>
```

#### Deploy
* Paso 1
```bash
mvn clean package -DskipTests
```
* Paso 2
```bash
mvn spring-boot:run
```
o también se puede utilizar
```bash
java -jar target/workshop-spring-boot-0.0.1-SNAPSHOT.jar
```

